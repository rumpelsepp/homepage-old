ASCIIDOC       ?= asciidoctor
ASCIIDOC_FLAGS ?= -a !stylesheet -a nofooter

ADOC_FILES = $(shell find . -type f -name '*.adoc')
HTML_FILES = $(patsubst %.adoc,%.html,$(ADOC_FILES))
INCLUDES   =
CSS        =

html: $(HTML_FILES)

%.html : %.adoc $(INCLUDES) $(CSS)
	$(ASCIIDOC) $(ASCIIDOC_FLAGS) $<

clean:
	$(RM) *.html
	$(RM) **/*.html

deploy:
	rsync -e ssh -rP --delete --exclude .git --exclude "*.adoc" . deploy@rumpelsepp.org:rumpelsepp.org/

.PHONY: clean deploy
